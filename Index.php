<html>

<h1>POVESTE</h1>

<body>
	
	<h3>Te trezesti intr-o temnita, intr-o celula pazita de doi paznici. Unul dintre ei se uita la tine, ranjind cu dispret.<br>
	La un moment dat, acesta iti spune: "Totul se va sfarsi pentru tine maine, talharule! Vei fi spanzurat la fel ca si ceilalti complici de-ai tai!"<br>
	Al doilea paznic ii spune: "Nu poate trece noaptea asta mai repede? Banditii astia imi dau fiori, pazind celulele lor!"<br>
	"Nu-ti face griji", spuse celalalt paznic, "odata ce-i executam pe toti talharii astia, vom putea sa dormim mai linistiti la noapte!"<br>
	Atunci iti vine in minte un lucru: esti parte a unui grup de banditi care ii jefuiau pe cavalerii si negustorii lacomi si zgarciti.<br>
	In ultima ta amubscada, ai fost luat prizonier alaturi de alti criminali de catre garda de elita a regelui.<br>
	In acea noapte, observi ca paznicii au adormit, lasand celula nepazita, si realizezi ca asta e singura ta sansa de a evada din temnita.<br>
	Fara a face vreun zgomot, furi cheia celulei de la unul dintre paznici si deschizi usa celulei.<br>
	Te furisezi pe langa celelalte celule in cautarea unei iesiri, pana cand descoperi o usa care duce spre culoarul temnitei.<br>
	Stiind ca e posibil ca alti calai sa patruleze pe acel culoar, ai acum de ales: </h3>
	
	
	<form action="Proiect_PHP_002(poveste_pt_1)_alegerea_1.php" method="post">
	
	<input type="submit" name="option_1" value="Iesi pe culoar si infrunta calaii">
	
	</form>
	
	<form action="Proiect_PHP_002(poveste_pt_1)_alegerea_2.php" method="post">
	
	<input type="submit" name="option_2" value="Asteapta pana e liniste totala">
	
	</form>
	
</body>

</html>